﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour {

    public GameObject player;

    [SerializeField]
    private PlayerMec playermec;

    private void OnCollisionEnter(Collision collisionInfo)
    {
        bool enemyDead = collisionInfo.collider.transform.root.GetComponent<Enemy>().GetDamage();
        if (collisionInfo.collider.tag == "Enemy")
        {
            if(!enemyDead)
                playermec.TakeDamage();
        }

    }
}
