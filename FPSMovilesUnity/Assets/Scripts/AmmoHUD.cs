﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoHUD : MonoBehaviour {

    public Gun gun;
    public int ammoMax;

    public Image[] bulletImages;
    public Sprite[] bulletSprites;

    // Update is called once per frame
    void Update () {
		 for (int i=0; i<ammoMax;i++)
        {
            if (gun.currentAmmo <= i)
            {
                bulletImages[i].enabled = false;
            }
            else
                bulletImages[i].enabled = true;
        }
    }
}
