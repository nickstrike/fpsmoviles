﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float health = 100f;
    //public static int count = 0;
    public Animator anim;
    public float fwdSpeed;
    public Transform player;
    public AudioSource audio;
    public AudioClip ughAudio;
    public AudioClip painAudio;

    //public float time;

    private bool isChasing = false;
    private bool dead = false;

    private void Awake()
    {
        //count++;
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        player = GameObject.FindWithTag("Player").transform;
    }

    private void Update()
    {
        var lookPos = player.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        if (!dead)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 100);
        }
        if (isChasing && !dead)
            transform.Translate(0, 0, fwdSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!dead && other.tag == "Atract")
        {
            isChasing = true;
        }
    }

    public void TakeDamage(float dmg)
    {
        if (!dead)
        {
            isChasing = true;
            anim.Play("getHit2");
            audio.PlayOneShot(ughAudio);
            health -= dmg;

            if (health <= 0f)
            {
                Die();
            }
        }
    }
    /*
        IEnumerator walkAnim(float _time)
        {

            anim.Play("walk");

            yield return new WaitForSeconds(_time);

        }*/

    IEnumerator Animate(float _time, string animName)
    {
        if(animName == "walk")
        {
            //code here
        }

        if (animName == "death")
        {
            dead = true;
            anim.Play(animName);
            audio.PlayOneShot(painAudio);
            yield return new WaitForSeconds(_time);
            Destroy(gameObject);
        }
    }

    void Die()
    {
        StartCoroutine(Animate(5, "death"));
        //count--;
    }

    public bool GetDamage()
    {
        if (!dead)
            return false;
        else
            return true;
    }

}
